package com.telus.sample.hackathonboilerplate.objects;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by garett on 2017-08-16.
 */

public class FileMetaData {
    public enum STATUS {
        SAVED,
        DELETED,
        EXPIRED,
        PENDING
    }

    public STATUS fileStatus;
    public String serverFileId;
    public String senderUserId;
    public String mimeType;
    URI fileUri;
    public long expirationTime;
    long startTime;

    public FileMetaData(String serverFileId, String senderUserId, STATUS fileStatus, long expirationTime) {
        this.serverFileId = serverFileId;
        this.senderUserId = senderUserId;
        this.fileStatus = fileStatus;
        this.expirationTime = expirationTime;
        if (expirationTime > new Date().getTime()) {
            this.fileStatus = STATUS.EXPIRED;
        }
    }

    /**
     * "id": "123",
     * "ownerId": "1111",
     * "recipientId": "2222",
     * "recipientType": "user",
     * "status": "pending",
     * "createdAt": "2016-05-26T20:27:24.351Z",
     * "expiresAt": "2016-05-26T20:27:24.351Z",
     * "url": "/files/123"
     *
     * @param fileMetaDataJson
     */

    public FileMetaData(JSONObject fileMetaDataJson) throws JSONException {
        this.serverFileId = fileMetaDataJson.getString("id");
        this.senderUserId = fileMetaDataJson.getString("userId");

        String expirationString = fileMetaDataJson.getString("expiredAt");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        try {
            Date date = dateFormat.parse(expirationString);
            this.expirationTime = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String statusString = fileMetaDataJson.getString("status");

        if (expirationTime <= new Date().getTime()) {
            this.fileStatus = STATUS.EXPIRED;
        } else if (statusString.equals("pending")) {
            this.fileStatus = STATUS.PENDING;
        }

    }


    public void setFileUri(URI fileUri) {
        this.fileUri = fileUri;
    }

    public JSONObject toJSONObject() throws JSONException {
        JSONObject fileMetaDataJSO = new JSONObject();
        fileMetaDataJSO.put("serverFileId", this.serverFileId);
        fileMetaDataJSO.put("senderUserId", this.senderUserId);
        fileMetaDataJSO.put("expirationTime", this.expirationTime);
        fileMetaDataJSO.put("startTime", this.startTime);
        fileMetaDataJSO.put("fileUri", this.fileUri.toString());
        fileMetaDataJSO.put("fileStatus", this.fileStatus.name());
        fileMetaDataJSO.put("mimeType", this.mimeType);
        return fileMetaDataJSO;
    }
}
