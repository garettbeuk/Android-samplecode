package com.telus.sample.hackathonboilerplate.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.telus.sample.hackathonboilerplate.AuthManager;
import com.telus.sample.hackathonboilerplate.R;
import com.telus.sample.hackathonboilerplate.adapters.ContactsAdapter;
import com.telus.sample.hackathonboilerplate.adapters.FileAdapter;
import com.telus.sample.hackathonboilerplate.adapters.GenericClickListener;
import com.telus.sample.hackathonboilerplate.crypto.CryptLib;
import com.telus.sample.hackathonboilerplate.networking.ControlPanel;
import com.telus.sample.hackathonboilerplate.objects.Contact;
import com.telus.sample.hackathonboilerplate.objects.FileMetaData;
import com.telus.sample.hackathonboilerplate.objects.network_responses.BaseResponse;
import com.telus.sample.hackathonboilerplate.objects.network_responses.FileDownloadResponse;
import com.telus.sample.hackathonboilerplate.objects.network_responses.GetFileListResponse;

import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class ContactInteractionScreen extends AppCompatActivity {

    AuthManager mAuthManager;
    int RESULT_LOAD_IMAGE = 44;

    RecyclerView mFileMetadataRecyclerView;
    FileAdapter mFileAdapter;
    List<FileMetaData> mFileMetadataList;

    String mUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_interaction_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mUserId = getIntent().getStringExtra("USER_ID");
        setTitle("Messagin' with " + "");

        mAuthManager = AuthManager.getInstance();

        mFileMetadataRecyclerView = (RecyclerView) findViewById(R.id.file_list_recycler_view);
        mFileMetadataRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mFileAdapter = new FileAdapter();
        mFileAdapter.setGenericClickListener(new GenericClickListener<FileMetaData>() {
            @Override
            public void onItemClick(FileMetaData fileMetaData, View view, int position) {
                ControlPanel.getFile(mAuthManager.getAuthToken().token, fileMetaData.serverFileId, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        FileDownloadResponse fileDownloadResponse = new FileDownloadResponse(response);
                        if (fileDownloadResponse.isOk) {
                            if (fileDownloadResponse.getFileMetaData().mimeType.equals("text/plain")) {
                                String stringMessage = new String(Base64.decode(fileDownloadResponse.getFileData(), Base64.NO_WRAP));

                                TextView textView = new TextView(ContactInteractionScreen.this);
                                textView.setText(stringMessage);
                                updateDumpField(textView);
                            } else if (fileDownloadResponse.getFileMetaData().mimeType.equals("image/jpg")) {
                                byte[] base64DecodedImage = Base64.decode(fileDownloadResponse.getFileData(), Base64.NO_WRAP);
                                Bitmap bitmap = BitmapFactory.decodeStream(new ByteArrayInputStream(base64DecodedImage));
                                ImageView imageView = new ImageView(ContactInteractionScreen.this);
                                imageView.setImageBitmap(bitmap);
                                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                                updateDumpField(imageView);
                            }
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
            }
        });


        findViewById(R.id.send_image_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent selectImageIntent = new Intent();
                selectImageIntent.setType("image/*");
                selectImageIntent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(selectImageIntent, "Select the image to send"), RESULT_LOAD_IMAGE);
            }
        });

        findViewById(R.id.send_text_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String textToSend = ((TextView) findViewById(R.id.text_to_send)).getText().toString();
                if (!textToSend.isEmpty()) {
                    String textPayload = "data:text/plain;base64," + Base64.encodeToString(textToSend.getBytes(), Base64.NO_WRAP);
                    String iv = CryptLib.generateRandomIV(16);
                    try {
                        CryptLib cryptLib = new CryptLib();
                        String encryptedTextPaylod = iv + cryptLib.encrypt(textPayload, "KEY", iv);//TODO retrieve actual session key
                        ControlPanel.uploadFile(mAuthManager.getAuthToken().token, mAuthManager.getAuthToken().userid, mUserId, encryptedTextPaylod, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                if (new BaseResponse(response).isOk) {
                                    Log.d("SUCCESS", "");
                                } else {
                                    Log.d("FAILED", new BaseResponse(response).message);
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("FAILED", error.getMessage());
                            }
                        });
                    } catch (InvalidKeyException | UnsupportedEncodingException | IllegalBlockSizeException | InvalidAlgorithmParameterException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e) {
                        e.printStackTrace();
                    }

                    ((TextView) findViewById(R.id.text_to_send)).setText("");
                }
            }
        });


        ControlPanel.getReceivedFiles(mAuthManager.getAuthToken().token, mAuthManager.getAuthToken().userid, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                GetFileListResponse getFileListResponse = new GetFileListResponse(response);
                if (getFileListResponse.isOk) {
                    List<FileMetaData> fileMetaDataList = getFileListResponse.getFileMetaDataList();
                    mFileMetadataList = new ArrayList<FileMetaData>();
                    for (FileMetaData fileMetaData : fileMetaDataList) {
                        if (fileMetaData.senderUserId.equals(mUserId)) {
                            mFileMetadataList.add(fileMetaData);
                        }
                    }
                    mFileAdapter.updateDataSet(mFileMetadataList);
                    mFileMetadataRecyclerView.setAdapter(mFileAdapter);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            String encodedImage = "";
            Uri uri = data.getData();

            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 10, byteArrayOutputStream);

                byte[] bitmapByteArray = byteArrayOutputStream.toByteArray();
                encodedImage = "data:image/jpg;base64," + Base64.encodeToString(bitmapByteArray, Base64.NO_WRAP);
                byteArrayOutputStream.close();

                String iv = CryptLib.generateRandomIV(16);
                CryptLib cryptLib = new CryptLib();
                String encryptedTextPaylod = iv + cryptLib.encrypt(encodedImage, "KEY", iv);//TODO retrieve actual session key

                String token = mAuthManager.getAuthToken().token;
                ControlPanel.uploadFile(token, mAuthManager.getAuthToken().userid, mUserId, encryptedTextPaylod, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("SUCCESS", response.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("FAIL", error.getMessage());
                    }
                });

            } catch (IOException | NoSuchAlgorithmException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | NoSuchPaddingException | IllegalBlockSizeException e) {
                e.printStackTrace();
            }
        }
    }

    private void updateDumpField(final View view) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((LinearLayout) findViewById(R.id.dump_layout)).removeAllViews();
                ((LinearLayout) findViewById(R.id.dump_layout)).addView(view);
            }
        });
    }
}
