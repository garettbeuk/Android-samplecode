package com.telus.sample.hackathonboilerplate.objects.network_responses;

import com.telus.sample.hackathonboilerplate.objects.Contact;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by garett on 2017-08-16.
 */

public class GetContactsResponse extends BaseResponse {

    private List<Contact> contactList = new ArrayList<>();

    public GetContactsResponse(JSONObject getContactsNetworkResponse) throws JSONException {
        super(getContactsNetworkResponse);
        if(!isOk){
            return;
        }

        JSONArray contactsListInJSON = getContactsNetworkResponse.getJSONArray("contacts");

        for (int i = 0 ; i < contactsListInJSON.length() ; ++i){
            Contact contact = Contact.parseFromJson(contactsListInJSON.getJSONObject(i));
            contactList.add(contact);
        }
    }

    public List<Contact> getContactList(){
        return this.contactList;
    }
}
