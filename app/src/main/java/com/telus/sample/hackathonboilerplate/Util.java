package com.telus.sample.hackathonboilerplate;

/**
 * Created by garett on 2016-06-15.
 */
public class Util {

    public static final String TOKENS = "tokens";
    public static final String ACCESS_TOKEN = "accessToken";
    public static final String REFRESH_TOKEN = "refreshToken";

    public static final String BASE_URL = "https://hack2017.mbenablers.com";
    public static final String LOGIN_URL = BASE_URL + "/tokens";


    public static final String LAUNCH_BUNDLE_DATA = "LAUNCH_BUNDLE_DATA";
}