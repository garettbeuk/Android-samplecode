package com.telus.sample.hackathonboilerplate.networking;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.telus.sample.hackathonboilerplate.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by garett on 2016-10-18.
 */

public class ControlPanel {


    private static final String BASE_URL = Util.BASE_URL;


    public static void authenticateUser(String username, String password, Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener) {

        int requestMethod = Request.Method.POST;
        String AUTH_URL = Util.LOGIN_URL;
        JSONObject credentials = new JSONObject();

        try {
            credentials.put("username", username);
            credentials.put("password", password);
            Log.d("authenticateUser", credentials.toString());
        } catch (JSONException e) {
            e.printStackTrace();
            errorListener.onErrorResponse(new VolleyError(e.getMessage()));
            return;
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(requestMethod, AUTH_URL, credentials, responseListener, errorListener);
        RequestQueueOven.getRequestQueue().add(jsonObjectRequest);
    }

    public static void refreshToken(String refreshToken, Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener) {

        int requestMethod = Request.Method.PUT;
        String AUTH_URL = BASE_URL + "/auth/tokens";

        JsonObjectRequest jsonObjectRequest = new AuthenticatedJsonObjectRequest(requestMethod, AUTH_URL, null, refreshToken, responseListener, errorListener);
        RequestQueueOven.getRequestQueue().add(jsonObjectRequest);
    }

    public static void getContactList(final String authToken, String userId, final Response.Listener<JSONObject> responseListener, final Response.ErrorListener errorListener) {

        int requestMethod = Request.Method.GET;
        String RESOURCE_URL = BASE_URL + "/users/" + userId + "/contacts";

        JsonObjectRequest jsonObjectRequest = new AuthenticatedJsonObjectRequest(requestMethod, RESOURCE_URL, null, authToken, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                responseListener.onResponse(response);
            }
        }, errorListener);

        RequestQueueOven.getRequestQueue().add(jsonObjectRequest);
    }

    /**
     * curl \
     * -X POST \
     * -H "Authorization: Bearer eyJ0eXAiOiJKV1.eyJqdGkiOiJhMDFU2.i9YYifTpQsmKWAG" \
     * -H "Content-Type: application/json" \
     * -d '{ "file": "[example file content data]", "recipientId": "2222" }' \
     * https://hack2017.mbenablers.com/users/1111/files
     */

    public static void uploadFile(final String authToken, String senderUserId, String recipientUserId, String base64EncodedFile, final Response.Listener<JSONObject> responseListener, final Response.ErrorListener errorListener) {

        int requestMethod = Request.Method.POST;
        String RESOURCE_URL = BASE_URL + "/users/" + senderUserId + "/files";

        JSONObject fileUploadPayload = new JSONObject();
        try {
            fileUploadPayload.put("file", base64EncodedFile);
            fileUploadPayload.put("recipientId", recipientUserId);
        } catch (JSONException e) {
            e.printStackTrace();
            errorListener.onErrorResponse(new VolleyError(e.getMessage()));
            return;
        }

        JsonObjectRequest jsonObjectRequest = new AuthenticatedJsonObjectRequest(requestMethod, RESOURCE_URL, fileUploadPayload, authToken, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                responseListener.onResponse(response);
            }
        }, errorListener);

        RequestQueueOven.getRequestQueue().add(jsonObjectRequest);
    }

    /**
     * curl \
     * -X POST \
     * -H "Authorization: Bearer eyJ0eXAiOiJKV1.eyJqdGkiOiJhMDFU2.i9YYifTpQsmKWAG" \
     * -H "Content-Type: application/json" \
     * https://hack2017.mbenablers.com/users/2222/files/received
     */

    public static void getReceivedFiles(final String authToken, String userId, final Response.Listener<JSONObject> responseListener, final Response.ErrorListener errorListener) {

        int requestMethod = Request.Method.GET;
        String RESOURCE_URL = BASE_URL + "/users/" + userId + "/files/received";

        JsonObjectRequest jsonObjectRequest = new AuthenticatedJsonObjectRequest(requestMethod, RESOURCE_URL, null, authToken, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                responseListener.onResponse(response);
            }
        }, errorListener);

        RequestQueueOven.getRequestQueue().add(jsonObjectRequest);
    }


    /**
     * curl \
     * -X GET \
     * -H "Authorization: Bearer eyJ0eXAiOiJKV1.eyJqdGkiOiJhMDFU2.i9YYifTpQsmKWAG" \
     * -H "Content-Type: application/json" \
     * https://hack2017.mbenablers.com/users/files/123
     */

    public static void getFile(final String authToken, String fileId, final Response.Listener<JSONObject> responseListener, final Response.ErrorListener errorListener) {

        int requestMethod = Request.Method.GET;
        String RESOURCE_URL = BASE_URL + "/files/" + fileId;

        JsonObjectRequest jsonObjectRequest = new AuthenticatedJsonObjectRequest(requestMethod, RESOURCE_URL, null, authToken, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                responseListener.onResponse(response);
            }
        }, errorListener);

        RequestQueueOven.getRequestQueue().add(jsonObjectRequest);
    }


    /**
     * curl \
     * -X DELETE \
     * -H "Authorization: Bearer eyJ0eXAiOiJKV1.eyJqdGkiOiJhMDFU2.i9YYifTpQsmKWAG" \
     * -H "Content-Type: application/json" \
     * https://hack2017.mbenablers.com/users/1111/files/123
     */

    public static void deleteFile(final String authToken, String userId, String fileId, final Response.Listener<JSONObject> responseListener, final Response.ErrorListener errorListener) {

        int requestMethod = Request.Method.DELETE;
        String RESOURCE_URL = BASE_URL + "/users/" + userId + "/files/" + fileId;

        JsonObjectRequest jsonObjectRequest = new AuthenticatedJsonObjectRequest(requestMethod, RESOURCE_URL, null, authToken, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                responseListener.onResponse(response);
            }
        }, errorListener);

        RequestQueueOven.getRequestQueue().add(jsonObjectRequest);
    }

}
