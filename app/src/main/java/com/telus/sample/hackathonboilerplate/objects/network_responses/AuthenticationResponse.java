package com.telus.sample.hackathonboilerplate.objects.network_responses;

import com.telus.sample.hackathonboilerplate.Util;

import org.json.JSONObject;

/**
 * Created by garett on 2017-08-16.
 */

public class AuthenticationResponse extends BaseResponse {

    private String accessToken;
    private String refreshToken;

    public AuthenticationResponse(JSONObject authenticateUserResponse) {
        super(authenticateUserResponse);
        if (!isOk) {
            return;
        }

        JSONObject token = authenticateUserResponse.optJSONObject(Util.TOKENS);
        if (token == null) {
            isOk = false;
            message = "Failed to parse token received";
            return;
        }
        String accessToken = token.optString(Util.ACCESS_TOKEN, "");
        String refreshToken = token.optString(Util.REFRESH_TOKEN, "");

        if (refreshToken.isEmpty() || accessToken.isEmpty()){
            isOk = false;
            message = "Failed to parse token received";
            return;
        }
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

}
