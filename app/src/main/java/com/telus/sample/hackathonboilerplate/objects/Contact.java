package com.telus.sample.hackathonboilerplate.objects;

import org.json.JSONObject;

/**
 * Created by antonio on 2017-08-16.
 */

public class Contact {


    private String userId;
    private String userName;
    private String commKey;
    private String sessionKey;

    public String getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public String getCommKey() {
        return commKey;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public static Contact parseFromJson(JSONObject jsonContact) {
        if (jsonContact == null) {
            return null;
        }
        Contact contact = new Contact();
        jsonContact = jsonContact.optJSONObject("user");
        contact.userId = jsonContact.optString("id");
        contact.userName = jsonContact.optString("username");
        contact.commKey = jsonContact.optString("cryptoKey");
        contact.sessionKey = jsonContact.optString("sessionKey");
        return contact;
    }
}
