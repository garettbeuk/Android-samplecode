package com.telus.sample.hackathonboilerplate;

import android.content.Context;
import android.content.SharedPreferences;

public class DataStore {
    public static String getToken(Context context) {
        return getString(context, Util.ACCESS_TOKEN);
    }

    static String getRefreshToken(Context context) {
        return getString(context, Util.REFRESH_TOKEN);
    }

    private static String getString(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("hackathon", 0);
        return sharedPreferences.getString(key, "");
    }

    public static void storeToken(Context context, String token) {
        storeString(context, Util.ACCESS_TOKEN, token);
    }

    static void storeRefreshToken(Context context, String token) {
        storeString(context, Util.REFRESH_TOKEN, token);
    }

    private static void storeString(Context context, String key, String value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("hackathon", 0);
        sharedPreferences.edit().putString(key, value).apply();
    }

    static void clearStore(Context context){
        context.getSharedPreferences("hackathon", 0).edit().clear().apply();
    }


}
