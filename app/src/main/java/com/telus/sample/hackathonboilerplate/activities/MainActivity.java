package com.telus.sample.hackathonboilerplate.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.telus.sample.hackathonboilerplate.AuthManager;
import com.telus.sample.hackathonboilerplate.R;
import com.telus.sample.hackathonboilerplate.Util;
import com.telus.sample.hackathonboilerplate.networking.RequestQueueOven;


public class MainActivity extends AppCompatActivity {


    AuthManager mAuthManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RequestQueueOven.instantiateRequestQueue(getApplicationContext());

        mAuthManager = AuthManager.getInstance();
        mAuthManager.setContext(getApplicationContext());

        Intent launchInent = getIntent();
        if (launchInent.getAction().equals("android.intent.action.MAIN")) { //Launched from launcher

            //If the token exists in memory then let's assume we're authenticated.
            if (mAuthManager.getAuthToken() != null && mAuthManager.getAuthToken().isValid()) {
                startActivity(ContactListActivity.class);
            } else {
                startActivity(LoginActivity.class);
            }
        } else if (launchInent.getScheme().equals("sec")) {
            Uri launchIntentPayload = launchInent.getData();
            String path = launchIntentPayload.getPath();
            Log.d("LAUNCH", path);
            if (path.startsWith("/cske")) {
                int secondSlash = path.indexOf('/', 2) + 1;
                String cutPath = path.substring(secondSlash);
                startActivityWithData(ContactSessionKeyEstablish.class, cutPath);

            } else if (path.startsWith("/inv")) {

            } else if (path.startsWith("/alert")) {

            }

        }
    }

    private void startActivityWithData(Class activityClass, String data) {
        Intent launchIntent = new Intent(getApplicationContext(), activityClass);
        if (!data.isEmpty()) {
            launchIntent.putExtra(Util.LAUNCH_BUNDLE_DATA, data);
        }
        startActivity(launchIntent);
    }

    private void startActivity(Class activityClass) {
        startActivityWithData(activityClass, "");
    }
}
