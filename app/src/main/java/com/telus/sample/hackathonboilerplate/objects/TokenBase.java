package com.telus.sample.hackathonboilerplate.objects;

import android.util.Base64;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by garett on 2016-10-27.
 */

public abstract class TokenBase {

    public long expirationTimeInMilliseconds;
    public String useremail;
    public String username;
    public String userid;
    public boolean userIsAdmin;

    public String token;

    private final static String EXPIRATION_KEY = "exp";
    private final static String USER_ID_KEY = "userId";
    private final static String USER_EMAIL_KEY = "email";
    private final static String USERNAME_KEY = "username";
    private final static String IS_ADMIN = "isAdmin";


    public TokenBase(String tokenString) throws JSONException, InstantiationException {

        if (tokenString == null || tokenString.isEmpty()) {
            throw new InstantiationException("Empty token provided");
        }

        this.token = tokenString;
        JSONObject jsonTokenPayload = parseJsonToken(tokenString);
        expirationTimeInMilliseconds = jsonTokenPayload.getLong(EXPIRATION_KEY) * 1000; //The expiration time is in seconds, we need it in milliseconds to convert to a valid date
        //useremail = jsonTokenPayload.getString(USER_EMAIL_KEY);
        username = jsonTokenPayload.getString(USERNAME_KEY);
        userid = jsonTokenPayload.getString(USER_ID_KEY);
        userIsAdmin = jsonTokenPayload.getBoolean(IS_ADMIN);
    }

    private JSONObject parseJsonToken(String tokenString) throws JSONException, IndexOutOfBoundsException {
        JSONObject jsonToken;

        //JWTs have 3 parts, the middle part includes the payload, and that's what we're after
        String[] tokenParts = tokenString.split("\\.");
        if (tokenParts.length != 3) {
            throw new IndexOutOfBoundsException("Invalid token received");
        }

        String stringJsonPayload = new String(Base64.decode(tokenParts[1], Base64.DEFAULT));

        jsonToken = new JSONObject(stringJsonPayload);
        return jsonToken;
    }


    //We don't have much of a way to validate the integrity of the token, so we'll rely on the
    public boolean isValid() {
        return expirationTimeInMilliseconds > new Date().getTime();
    }


}
