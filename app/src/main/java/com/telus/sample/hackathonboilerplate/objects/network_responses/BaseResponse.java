package com.telus.sample.hackathonboilerplate.objects.network_responses;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by garett on 2017-08-16.
 */

public class BaseResponse {

    public boolean isOk;
    public String message;

    public BaseResponse(JSONObject networkResponse) {
        this.isOk = networkResponse.optBoolean("ok");
        this.message = networkResponse.optString("message", "No message...weird");
    }


}
