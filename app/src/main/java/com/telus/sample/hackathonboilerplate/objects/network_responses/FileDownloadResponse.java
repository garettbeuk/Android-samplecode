package com.telus.sample.hackathonboilerplate.objects.network_responses;

import com.telus.sample.hackathonboilerplate.ContactManager;
import com.telus.sample.hackathonboilerplate.crypto.CryptLib;
import com.telus.sample.hackathonboilerplate.objects.Contact;
import com.telus.sample.hackathonboilerplate.objects.FileMetaData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * Created by garett on 2017-08-17.
 */

public class FileDownloadResponse extends BaseResponse {

    private FileMetaData fileMetaData;
    private byte[] fileData;

    public FileDownloadResponse(JSONObject fileDownloadResponseJSON) {
        super(fileDownloadResponseJSON);

        JSONObject fileResponse = fileDownloadResponseJSON.optJSONObject("file");
        try {
            fileMetaData = new FileMetaData(fileResponse);

        } catch (JSONException e) {
            fileMetaData = null;
        }

        decryptFile(fileResponse.optString("data"));

    }

    private void decryptFile(String fileString) {
        //Contact contact = ContactManager.getContactById(fileMetaData.senderUserId);
        String key = "KEY";//contact.getSessionKey();
        String iv = fileString.substring(0, 16);
        String encryptedData = fileString.substring(16);
        try {
            CryptLib cryptLib = new CryptLib();
            String decryptedData = cryptLib.decrypt(encryptedData, key, iv);
            String meta = decryptedData.substring(0, decryptedData.indexOf(','));
            decryptedData = decryptedData.substring(decryptedData.indexOf(',') + 1);
            if (meta.contains("text/plain")) {
                fileMetaData.mimeType = "text/plain";
            } else if (meta.contains("image/jp")) {
                fileMetaData.mimeType = "image/jpg";
            }

            fileData = decryptedData.getBytes();

        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }

    public FileMetaData getFileMetaData() {
        if (isOk) {
            return fileMetaData;
        }
        return null;
    }

    public byte[] getFileData() {
        return fileData;
    }


}
