package com.telus.sample.hackathonboilerplate.adapters;

import android.view.View;

/**
 * Created by garett on 2017-01-17.
 */

public interface GenericClickListener<T> {

    void onItemClick(T objectClicked, View view, int position);
}
