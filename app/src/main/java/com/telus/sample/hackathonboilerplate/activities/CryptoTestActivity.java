package com.telus.sample.hackathonboilerplate.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.telus.sample.hackathonboilerplate.R;
import com.telus.sample.hackathonboilerplate.crypto.CryptLib;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class CryptoTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crypto_test);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        findViewById(R.id.encryptButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String plainText = ((TextView)findViewById(R.id.plainTextEditView)).getText().toString();
                String cryptoKeyText = ((TextView)findViewById(R.id.cryptoKeyTextView)).getText().toString();
                if(cryptoKeyText.isEmpty()){
                    ((TextView)findViewById(R.id.cryptoKeyTextView)).setHint("You need to put stuff here");
                    return;
                }

                if(plainText.isEmpty()){
                    ((TextView)findViewById(R.id.plainTextEditView)).setHint("You need to put stuff here");
                    return;
                }

                String iv = CryptLib.generateRandomIV(16); //128-bit IV for 256-bit AES
                String key;
                try {
                    key = CryptLib.SHA256(cryptoKeyText,32); //256-bit key (32 bytes, 8 bits in a byte, so 256-bits) #MATH!
                } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return;
                }

                String encryptedText;
                try {
                    encryptedText = new CryptLib().encrypt(plainText, key, iv);
                } catch (InvalidKeyException | UnsupportedEncodingException | IllegalBlockSizeException | InvalidAlgorithmParameterException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e) {
                    e.printStackTrace();
                    return;
                }

                ((TextView)findViewById(R.id.encryptedTextEditView)).setText(encryptedText);

            }
        });


    }

    private void toasty(String message){

    }

}
