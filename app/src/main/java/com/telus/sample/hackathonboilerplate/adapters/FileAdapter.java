package com.telus.sample.hackathonboilerplate.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.telus.sample.hackathonboilerplate.R;
import com.telus.sample.hackathonboilerplate.objects.Contact;
import com.telus.sample.hackathonboilerplate.objects.FileMetaData;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class FileAdapter extends RecyclerView.Adapter<FileAdapter.ViewHolder> {

    private static String TAG = "ContactAdapter";

    private List<FileMetaData> mFileMetaDataList;

    private static GenericClickListener<FileMetaData> mGenericClickListener;

    public FileAdapter() {
        mFileMetaDataList = new ArrayList<>();
    }

    public void updateDataSet(List<FileMetaData> fileMetaDataList) {
        mFileMetaDataList = fileMetaDataList;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView mUsernameTextview;
        TextView mUserStatusTextView;

        ViewHolder(View rowBox) {
            super(rowBox);
            rowBox.setOnClickListener(this);
            mUsernameTextview = (TextView) rowBox.findViewById(R.id.contact_username_list_textview);
            mUserStatusTextView = (TextView) rowBox.findViewById(R.id.contact_user_status_textview);
        }

        @Override
        public void onClick(View v) {
            Log.d(TAG, "onClick: ");
            FileMetaData fileMetaData = mFileMetaDataList.get(getAdapterPosition());
            mGenericClickListener.onItemClick(fileMetaData, v, getAdapterPosition());
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_list_item_container, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        FileMetaData fileMetaData = mFileMetaDataList.get(position);
        holder.mUsernameTextview.setText("Expires: " + new Date(fileMetaData.expirationTime).toGMTString());
        holder.mUserStatusTextView.setText(fileMetaData.fileStatus.name());
    }

    @Override
    public int getItemCount() {
        return mFileMetaDataList.size();
    }

    public void setGenericClickListener(GenericClickListener<FileMetaData> genericClickListener) {
        mGenericClickListener = genericClickListener;
    }

    public void removeItemAt(int index) {
        if (mFileMetaDataList.size() >= index) {
            mFileMetaDataList.remove(index);
        }
    }
}
