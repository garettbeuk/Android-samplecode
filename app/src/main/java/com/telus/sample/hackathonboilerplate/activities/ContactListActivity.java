package com.telus.sample.hackathonboilerplate.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.telus.sample.hackathonboilerplate.AuthManager;
import com.telus.sample.hackathonboilerplate.R;
import com.telus.sample.hackathonboilerplate.adapters.ContactsAdapter;
import com.telus.sample.hackathonboilerplate.adapters.GenericClickListener;
import com.telus.sample.hackathonboilerplate.crypto.CryptLib;
import com.telus.sample.hackathonboilerplate.networking.ControlPanel;
import com.telus.sample.hackathonboilerplate.objects.AuthToken;
import com.telus.sample.hackathonboilerplate.objects.Contact;
import com.telus.sample.hackathonboilerplate.objects.network_responses.GetContactsResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class ContactListActivity extends AppCompatActivity {

    private String mToken;

    RecyclerView mContactRecyclerView;
    ContactsAdapter mContactAdapter;
    List<Contact> mContactList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final AuthToken authToken = AuthManager.getInstance().getAuthToken();
        if (!authToken.isValid()) {
            finish();
        }
        mToken = authToken.token;

        mContactAdapter = new ContactsAdapter();
        mContactAdapter.setGenericClickListener(new GenericClickListener<Contact>() {
            @Override
            public void onItemClick(Contact contact, View view, int position) {
                Contact selectedContact = mContactList.get(position);
                if (selectedContact.getSessionKey().isEmpty()){
                    //TODO generate session key, store then let the user share it
                    String shareSessionKey = generateSessionKey(selectedContact);
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Session Key");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareSessionKey);
                    startActivity(Intent.createChooser(sharingIntent, "Send to designated contact"));
                } else {
                    Intent launchContactScreen = new Intent(getApplicationContext(), ContactInteractionScreen.class);
                    launchContactScreen.putExtra("USER_ID", contact.getUserId());
                    startActivity(launchContactScreen);

                }
            }
        });

        mContactRecyclerView = (RecyclerView) findViewById(R.id.contact_list_recycler_view);
        mContactRecyclerView.setHasFixedSize(true);
        mContactRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mContactRecyclerView.setAdapter(mContactAdapter);

        ControlPanel.getContactList(mToken, authToken.userid, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    GetContactsResponse getContactsResponse = new GetContactsResponse(response);
                    if (getContactsResponse.isOk){
                        mContactList = getContactsResponse.getContactList();
                        mContactAdapter.updateDataSet(mContactList);
                        mContactRecyclerView.setAdapter(mContactAdapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("Error", e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error", error.getMessage());
            }
        });
    }

    private String generateSessionKey(Contact contact){
        String iv = CryptLib.generateRandomIV(16);
        String key = CryptLib.generateRandomIV(32);
        contact.setSessionKey(key);
        try {
            CryptLib cryptLib = new CryptLib();
            String encryptedKey = cryptLib.encrypt(key, contact.getCommKey(), iv);
            String senderUserId = AuthManager.getInstance().getAuthToken().userid;
            String sessionKeyShareFormat = "sec://ure/cske/" + iv + "." + senderUserId + "." + encryptedKey;
            return sessionKeyShareFormat;
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.logout) {
            AuthManager.getInstance().setContext(this).clearCredentialStore();
            finish();
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
