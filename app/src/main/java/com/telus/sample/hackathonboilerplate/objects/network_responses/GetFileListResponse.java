package com.telus.sample.hackathonboilerplate.objects.network_responses;

import com.telus.sample.hackathonboilerplate.ContactManager;
import com.telus.sample.hackathonboilerplate.crypto.CryptLib;
import com.telus.sample.hackathonboilerplate.objects.Contact;
import com.telus.sample.hackathonboilerplate.objects.FileMetaData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * Created by garett on 2017-08-17.
 */

public class GetFileListResponse extends BaseResponse {

    List<FileMetaData> fileMetaDataList = new ArrayList<>();

    public GetFileListResponse(JSONObject getFileListResponseJSON) {
        super(getFileListResponseJSON);

        JSONArray fileArray = getFileListResponseJSON.optJSONArray("files");

        if (fileArray == null){
            return;
        }

        for (int i = 0 ; i < fileArray.length() ; ++i){
            try {
                FileMetaData fileMetaData = new FileMetaData(fileArray.getJSONObject(i));
                fileMetaDataList.add(fileMetaData);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public List<FileMetaData> getFileMetaDataList() {
        if (isOk) {
            return fileMetaDataList;
        }
        return null;
    }


}
