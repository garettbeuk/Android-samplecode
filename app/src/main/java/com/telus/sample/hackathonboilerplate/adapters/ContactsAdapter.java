package com.telus.sample.hackathonboilerplate.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.telus.sample.hackathonboilerplate.R;
import com.telus.sample.hackathonboilerplate.objects.Contact;

import java.util.ArrayList;
import java.util.List;


public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder> {

    private static String TAG = "ContactAdapter";

    private List<Contact> mContactList;

    private static GenericClickListener<Contact> mGenericClickListener;

    public ContactsAdapter() {
        mContactList = new ArrayList<>();
    }

    public void updateDataSet(List<Contact> contactList) {
        mContactList = contactList;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView mUsernameTextview;
        TextView mUserStatusTextView;

        ViewHolder(View rowBox) {
            super(rowBox);
            rowBox.setOnClickListener(this);
            mUsernameTextview = (TextView) rowBox.findViewById(R.id.contact_username_list_textview);
            mUserStatusTextView = (TextView) rowBox.findViewById(R.id.contact_user_status_textview);
        }

        @Override
        public void onClick(View v) {
            Log.d(TAG, "onClick: ");
            Contact contact = mContactList.get(getAdapterPosition());
            mGenericClickListener.onItemClick(contact, v, getAdapterPosition());
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_list_item_container, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Contact contact = mContactList.get(position);
        holder.mUsernameTextview.setText(contact.getUserName());
        if (contact.getSessionKey().isEmpty()) {
            holder.mUserStatusTextView.setText("NO SESSION KEY");
        }
    }

    @Override
    public int getItemCount() {
        return mContactList.size();
    }

    public void setGenericClickListener(GenericClickListener<Contact> genericClickListener) {
        mGenericClickListener = genericClickListener;
    }

    public void removeItemAt(int index) {
        if (mContactList.size() >= index) {
            mContactList.remove(index);
        }
    }
}
