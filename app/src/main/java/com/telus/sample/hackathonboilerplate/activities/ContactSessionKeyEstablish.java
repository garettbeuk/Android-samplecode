package com.telus.sample.hackathonboilerplate.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.telus.sample.hackathonboilerplate.ContactManager;
import com.telus.sample.hackathonboilerplate.R;
import com.telus.sample.hackathonboilerplate.Util;
import com.telus.sample.hackathonboilerplate.crypto.CryptLib;
import com.telus.sample.hackathonboilerplate.objects.Contact;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class ContactSessionKeyEstablish extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_session_key_establish);

        /**
         *
         * split the CSKE data
         * IV.userID.encryptedSessionkeywexpirationlong
         *
         *
         */

        String pathSegment = getIntent().getStringExtra(Util.LAUNCH_BUNDLE_DATA);
        Log.d("pathSegment", pathSegment);

        //JWTs have 3 parts, the middle part includes the payload, and that's what we're after
        String[] dataParts = pathSegment.split("\\.");
        if (dataParts.length != 3) {
            throw new IndexOutOfBoundsException("Invalid token received");
        }
        String iv = dataParts[0];
        String userId = dataParts[1];
        String encryptedPayload = dataParts[2];


       /* Contact contact = ContactManager.getInstance().getContactById(userId);
        if (contact == null) {
            ((TextView)findViewById(R.id.cske_result_textview)).setText("User does not appear to be a contact");
            ((TextView)findViewById(R.id.cske_contact_detail_textview)).setText("User ID: " + userId);
            return;
        }*/

       findViewById(R.id.cske_exit_button).setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               finish();
           }
       });

        try {
            String decryptedPayload = new CryptLib().decrypt(encryptedPayload, "5027c1691cbf5dd52dd729b791df8ee30bc50195cc4ba8cd73bea4106da07c95", iv);
            //contact.setSessionKey(decryptedPayload);
            //TODO persist contact to storage
            ((TextView)findViewById(R.id.cske_result_textview)).setText("Successfully saved session key");
            ((TextView)findViewById(R.id.cske_contact_detail_textview)).setText("for user: " + "");
        } catch (InvalidKeyException | UnsupportedEncodingException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException | NoSuchPaddingException e) {
            e.printStackTrace();
        }

    }

}
