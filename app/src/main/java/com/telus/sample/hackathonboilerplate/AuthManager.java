package com.telus.sample.hackathonboilerplate;

import android.content.Context;

import com.telus.sample.hackathonboilerplate.objects.AuthToken;
import com.telus.sample.hackathonboilerplate.objects.RefreshToken;

import org.json.JSONException;

/**
 * Created by garett on 2016-10-27.
 */

public class AuthManager {

    private static AuthManager authManager;

    private AuthToken authToken;

    private RefreshToken refreshToken;

    private Context context;

    public static AuthManager getInstance() {
        if (authManager == null) {
            authManager = new AuthManager();
        }
        return authManager;
    }


    public AuthToken getAuthToken() {
        return authToken;
    }

    public AuthManager setContext(Context context) {
        if (this.context != null) {
            return this;
        }

        this.context = context;
        if (authToken == null) {
            try {
                String stringAuthToken = DataStore.getToken(context);
                authToken = new AuthToken(stringAuthToken);
                String stringRefreshToken = DataStore.getRefreshToken(context);
                refreshToken = new RefreshToken(stringRefreshToken);
            } catch (InstantiationException | JSONException e) {
                e.printStackTrace();
            }
        }
        return this;
    }

    public void setAuthToken(AuthToken authToken) {
        this.authToken = authToken;
        if (context != null) {
            DataStore.storeToken(context, authToken.token);
        }
    }

    public void setAuthToken(String stringAuthToken) throws JSONException, InstantiationException {
        setAuthToken(new AuthToken(stringAuthToken));
    }

    public RefreshToken getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(RefreshToken refreshToken) {
        this.refreshToken = refreshToken;
        if (context != null) {
            DataStore.storeRefreshToken(context, refreshToken.token);
        }
    }

    public void setRefreshToken(String stringRefreshToken) throws JSONException, InstantiationException {
        setRefreshToken(new RefreshToken(stringRefreshToken));
    }

    public boolean clearCredentialStore() {
        authToken = null;
        refreshToken = null;
        if (context != null) {
            DataStore.clearStore(context);
            return true;
        }
        return false;
    }

}
